---
sidebar: auto
---

# Advanced

## Head to wind & 720

After a rabbit start, head to wind on whistle, holding position, sitted on windward side, heeling to windward, keeping control with the rudder, shoulder stopping boom from hitting. Second whistle bear away and 720. After repetition, change tacks.

## 3 Tacks & 720

After a rabbit start, on each whistle must tack, repeat 3 times, and then double whistle 720. That makes one 720 on starboard, one 720 on port. Keep going and reset with a rabbit start using the last boat as a rabbit.

## Tack & hold changing hands

After rabbit start, tack on whistle but don't change hands, keep sailing on new tack with hands tiller extension on the back until next whistle allows to change hands to normal position. Must be able to sail with hands crossed, and that forces to ease sail on heavy wind, and to hike of course.

## Tack on 3 steps

Rabbit start. First whistle step leeward but keep sailing, heeling the boat. Second whistle go head to wind sitted on windward rail and hold. Third whistle, tack.

## Gybe on 3 steps.

Following the coach boat making a line, 3 2 1 start downwind all together. First whistle stand up, big step to leeward and mainsheet hand grabbing all mainsheet's lines close to the boom. Second whistle, sit on windward side and gybe, keep there. Third whistle, change sides with big step, and change hands last.

## Never ending

With a windward leeward course set, at the moment the fleet is coming downwind, they must use the coach boat as leeward mark. Is better to use a dragging buoy to have a buffer space in case they need to avoid crashing.
Then, after rounding the coach boat as leeward mark they go back to the windward mark and so on. Usually we keep this non stop for 30 minutes. 
To add difficulty the coach can move to the sides, so the upwind is not centered, they must be able to focus on boathandling and position at the same time.

Add extra difficulty taking two turns penalty on whistle, remark the penalty should be taken inmediatly and make it in almost impossible situations.

## Short - long windward mark

A windward leeward course is set with a two marks starting line. There are two windward marks, one very short and a second with a normal lenght. The course is a start, first short windward mark, gate, long windward mark, gate-finish. The short windward mark is moved between races to force match start tactic with upwind strategy. It is good to have a small, hard to find mark for this.

## Starts

The boathandling exercises are the foundation for a good start because you need to be able to position the boat exactly like you want to have a good start. But, starting on big fleets can be the most challenging problem. Starting excercises are oriented to manage time, distance, geometry, in the middle of boat handling restrictions and difficult situations.

### Surprise start

Two marks layout for a starting line in a windward leeward course. Fleet should be rounding the starting marks and at some point the coach makes a sound, and they must start. There is no line, so they start just going to close hauled. The objective is to keep a gap with the forward boat learning this gap will be the leeward space we need to keep on the lane after the start.

### Box start

Same layout, the start is restricted by the starting line and the 'wind line' on each mark. They must be inside the box all pre start time. Being out of the box makes a one turn penalty to be able to enter again.

### 360 at 30 seconds

Every 30 seconds they need to take one turn penalty. At 2 minutes, 1:30 ... last is at 0:30 because at start is no penalty.
The displacement across the line with light winds or heavy winds becomes very difficult so they need to think in advance about positioning.

### Splitted starts

The starting line is divided in two setting a smaller mark at the middle, this smaller mark can be dragged windward or leeward, making different favoured points at the start. Downwind they must round this middle mark as leeward mark.

