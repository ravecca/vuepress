---
sidebar: auto
---

# Beginners

This group is related to a 'learn to sail program', usually young kids. The approach is on practice, hands on, more than a theorical one. Light wind venues are perfect.
We are looking for a link between boat behaviour and feelings trying to 'listen' the natural movements of the boat.

Marks work as containers, and help the group to be together because they have the visual references, and the coach can move freely without beeing chased. 

## Two marks

Lay two marks at a distance according to the number of boats, in a way they have enought space to move without issues and they can sail at 90 degrees with the wind. All boats following the same direction, tacking on the starboard side mark, and gybing on the port side mark.

::: danger Be carefull
At gybing mark be sure you explain how to avoid being hit on the head. Use this drill with light wind.
:::

![drill1](/vuepress/drill1.png) 

The coach will be around the fleet talking about the maneuvers (first at gybe end, remembering kids to avoid being hit on the head by boom) but also about how to experience acceleration and how tell tails work. Also is good to make the same phisical 'gesture' like sailing on each part of the drill so kids can mimic and copy. 

![drill1b](/vuepress/drill1b.png) 

With a third mark, we are looking for a starboard tack closer to the wind, they need to link the 'pull in gesture' with the 'head up' to the wind action. At that mark they need to ease the sail out to bear away so the coach can talk about the importance to do that to allow the boat head down easely without forcing the rudder.

The mark can be gradually set closer to the wind, making the starboar tack close to a close hauled course.

::: warning Tip
For more advanced the starboard tack can be transformed on and upwind, and the reach on a dead downwind
:::