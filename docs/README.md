---
home: true
heroImage: /opti.png
footer: Diego
---

## Training session structure

In the day-to-day training, different drills will be executed in the water. It is important to be clear about how to divide the session, which will obviously be modified according to the stage of training we are in, the training group, etc. It will not be the same for a beginner group as for a champ team. But with this caveat, we can say a training day, can be summarized as follows, specifically based on the management of available time.

* **Rigging** :rocket: It is the first stage where the boat and the elements to train (clothes, water, food, etc.) are prepared. It is important to reduce the time (and energy) invested in this stage, building a method. A good guideline is to communicate the time when it is expected to have the team ready, with all set and ready to launch.
* **Brief** :pencil: Before going out on the water, the objectives of the day are clearly communicated and in chronological order what is going to be done, what is the plan. It even communicates possible alternative plans according to factors that can be found.
* **Practice** :sailboat: Sailing session.
* **Debrief** :tv: Upon returning from the water, a chalk talk is made where issues observed on the day are reviewed, videos are reviewed, and the plan to be followed is briefly recalled.
* **Distension** :ping_pong: Team-building activities such as games on land, or social activities may be included in the plan.