---
sidebar: auto
---

# Games

Games are used to engage the group, break an intensive training day, wait for more wind, etc.

## Optibasket (bottle game)

No marks. Two teams must drop a plastic water bottle inside the coach boat to make a goal. When a player picks up the bottle, must pass it before 3 seconds, or make an autopass farther than 3 boats lengh. They must respect rules and penalties.
Breaking this rules result on invalid goals or other penalty.

::: danger Be carefull
Use only on light winds. Competition can become too agresive, remind to avoid crashing and respect the rules
:::

Sailors must move on unpredictable ways and uncommon positions to gain a goal.
Bottle must be 1/3 filled or you can use a tennis ball or other, but bottle is easy to grab and throw.

## Missile

Strategy game first sailed at Alameda.

Set a windward leeward course, and start a race. Each player has one missile to launch before the start, one at the upwind leg, one at the downwind leg. Missile is used announcing loud who is the target, who must take a 720 promptly with a limit of two per leg. (= if receive more than two missiles only take two 720).
Everyone must try to win the race.

## Pair sailing

Make teams of two. They must sail attached with the bowline. Make a race with a windward leeward or other course. 

Let them find the problems for this setup:
* How to sail without covering the team mate.
* What append with the right of way if team is on different tacks.
* How to agree with decisions about where to go / tactics.

::: warning Good for team building
This drill or game is very good to mix experienced sailors with beginners, setting up one experienced with one beginner.
:::
