module.exports = {
    title: 'Sailing ❤️ drills',
    description: 'Coaching notes and sailing drills, all levels',
    base: '/vuepress/',
    dest: 'public',
    themeConfig: {
        nav: [
          { text: 'Home', link: '/' },
          { text: 'Beginners', link: '/beginners/' },
          { text: 'Intermediate', link: '/intermediate/' },
          { text: 'Advanced', link: '/advanced/' },
          { text: 'Games', link: '/games/' },
          { text: 'Resources', link: '/resources/' } 
        ],
        sidebar: 'auto'
      }
}