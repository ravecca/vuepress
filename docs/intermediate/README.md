---
sidebar: auto
---
# Intermediate

This group challenges are:
* Consistent technique at all courses
* Upwind sail trim
* Hiking and strong wind technique

## Rabbit start

The rabbit start is the very basic tool for regroup to follow any other drill. So it is very important the fleet understand the moves to get it done. The coach boat can be used as rabbit to regroup and to be more clear it can drag a buoy that kids associate with objective to be rounded.

## Follow the leader

All fleet follows the coach boat on single line, if someone goes out of the line, or overpass front boat, must go to the end.
Coach will manage speed and direction, and sailors will need to accelerate or stop according to space available. The easyest is 90 degrees to the wind and the harder will be downwind. Good for mimic starting line situation, and learn to control the boat.

## Up & down

After setting a single line following coach boat (follow the leader), with whitle will alternate downwind / upwind. It is important to remark it is not included gybes or tacks, everyone will be on the same tack during all time.

Very good to practice the two extremes on sail positioning: close hauled boom end over the corner, downwind boom 90 degrees with the boat.

Coach must follow with body mimics triming in, trying to keep things black and white, no greys.
## Square course drill

Setting a square four marks course, the objective is to win after three laps, and the key is that at every mark first boat takes a one turn penalty.
The better and smaller is the fleet, the shorter is the course. With strong wind it can be a triangle with three marks. On this course it is allowed to touch a mark.

Wind lines (dotted green lines) are used to say who is first on each mark. It doesn't matter how close or fare a boat is from a mark, what does matter is who touches that 'wind line' first.

![drill square](/vuepress/drill-square.png) 

So, first boat on each mark takes a 360, they learn very quick that is not a good deal being first, then first boats stop at each mark and the fleet gets condensed resulting on very interesting mark roundings.

The coach can be on the square center and give directions to kids on every leg of the course.

### Marks 1 & 2

Is easy to stop heading to the wind because there is a lot of space to use. Boats are piling up but is no problem to go upper to windward.

### Mark 3

Being the outside boat is paid very expensive on this mark. It resemble what happens on last leeward mark at trapezoid used on lasers, or every leeward mark on a big fleet.
That is why the stop is done on the inside side. It runs very fast with no space, and fast gybes are necessary to keep on a high chance spot.

### Mark 4

On similar way, this mark is difficult because to win, they need to stop thinking on being the inside boat, the mark has a no turning point doing that because gybing is more tight, and also, it is very hard to take a penalty on the last mark and win.

